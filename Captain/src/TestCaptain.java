//import Captain;

public class TestCaptain {

	public static void main(String[] args) {
		// Erzeugen der Objekte
		Captain cap1 = new Captain("Jean Luc ", "Picard", 2364, 4500.0);
		Captain cap2 = new Captain("Azetbur ", "Gorkon", 2373, 43.0);
		Captain cap3 = new Captain("James Tiberius ", "Kirk", 2270, 2500.0);
		Captain cap4 = new Captain("Jonathan ", "Archer", 2251, 750.5);
		Captain cap5 = new Captain("Christopher ", "Pike", 2257, 2200.75);
		Captain cap6 = new Captain("Luca ", "Francesconi", 2700, 350.34);

		// Bildschirmausgabe
		System.out.println("Name: " + cap1.getSurname());
		System.out.println("Vorname: " + cap1.getName());
		System.out.println("Kapitän seit: " + cap1.getCaptainYears());
		System.out.println("Gehalt: " + cap1.getSalary() + " Föderationsdukaten");
		System.out.println("Vollname: " + cap1.vollname());
		System.out.println(cap1 + "\n"); // Die toString() Methode wird aufgerufen
		System.out.println("\nName: " + cap2.getSurname());
		System.out.println("Vorname: " + cap2.getName());
		System.out.println("Kapitän seit: " + cap2.getCaptainYears());
		System.out.println("Gehalt: " + cap2.getSalary() + " Darsek");
		System.out.println("Vollname: " + cap2.vollname());
		System.out.println(cap2 + "\n"); // Die toString() Methode wird aufgerufen
		System.out.println("\nName: " + cap3.getSurname());
		System.out.println("Vorname: " + cap3.getName());
		System.out.println("Kapitän seit: " + cap3.getCaptainYears());
		System.out.println("Gehalt: " + cap3.getSalary() + " Föderationsdukaten");
		System.out.println("Vollname: " + cap3.vollname());
		System.out.println(cap3 + "\n");
		System.out.println("Name: " + cap4.getSurname());
		System.out.println("Vorname: " + cap4.getName());
		System.out.println("Kapitän seit: " + cap4.getCaptainYears());
		System.out.println("Gehalt: " + cap4.getSalary() + " Föderationsdukaten");
		System.out.println("Vollname: " + cap4.vollname());
		System.out.println(cap4 + "\n"); // Die toString() Methode wird aufgerufen
		System.out.println("\nName: " + cap5.getSurname());
		System.out.println("Vorname: " + cap5.getName());
		System.out.println("Kapitän seit: " + cap5.getCaptainYears());
		System.out.println("Gehalt: " + cap5.getSalary() + " Darsek");
		System.out.println("Vollname: " + cap5.vollname());
		System.out.println(cap5 + "\n");
		System.out.println("\nName: " + cap6.getSurname());
		System.out.println("Vorname: " + cap6.getName());
		System.out.println("Kapitän seit: " + cap6.getCaptainYears());
		System.out.println("Gehalt: " + cap6.getSalary() + " Darsek");
		System.out.println("Vollname: " + cap6.vollname());
		System.out.println(cap6 + "\n");
	}

}