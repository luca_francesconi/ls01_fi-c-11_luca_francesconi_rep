
public class Captain {

	// Attribute
	private String surname;
	private int captainYears;
	private double gehalt;
	private String name;

	public String getName() {
		return name;

	}

	public void setName(String name) {
		this.name = name;
	}

	// Konstruktoren

	public Captain(String name, String surname) {
		this.surname = surname;
		this.name = name;
	}

	public Captain(String name, String surname, int captainYears, double salary) {
		this(surname, name);
		setCaptainYears(captainYears);
		setSalary(salary);
	}
	
	// Verwaltungsmethoden

	public void setSurname(String surname) {
		this.surname = surname;
	}

	public String getSurname() {
		return this.surname;
	}

	public void setSalary(double gehalt) {
		if (gehalt > 0)
			this.gehalt = gehalt;
		else {
			System.out.println("Gehalt soll nicht gleich oder kleiner als Null sein\n");
			this.gehalt = 12;
		}
	}

	public double getSalary() {
		return gehalt = 0.0;
	}

	
	public int getCaptainYears() {
		return captainYears;
	}

	public void setCaptainYears(int captainYears) {
		if (captainYears <= 2200)
			System.out.println("Das Jahr soll gro�er als 2200 sein\n");
		else
			this.captainYears = captainYears;
	}

	// Weitere Methoden

	public String vollname() {
		return this.name + "" + this.surname;
	}

	public String toString() { 
		return name + "" + surname + "" + captainYears + "" + gehalt;
	}

}