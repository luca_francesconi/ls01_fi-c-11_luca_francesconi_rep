import java.util.Scanner;

public class MittelwertMitMethoden {

	public static void main(String[] args) {
		double summeDerEingaben = 0;
		double m;
		int anzahlWerte;
		
		Scanner myScanner = new Scanner(System.in);
		programmhinweis ("Dieses Programm berechnet den Mittelwert zweier zahl");
		anzahlWerte = eingabeAnzahl (myScanner, "Geben Sie die Anzahl der einzugebenden Zahl");
		
		for(int i = 0; i < anzahlWerte; i++) {
			summeDerEingaben += eingabeDouble (myScanner, "Neuer Wert eingeben: ");
			
		}
		
		m = mittelwertBerechnung(summeDerEingaben, anzahlWerte);
		
		
		ausgabe(m);
		
		myScanner.close();
	}
	
	public static void programmhinweis(String text) {
		System.out.println(text);
	}
	
	public static int eingabeAnzahl (Scanner ms, String text) { 
		System.out.println(text);
		int zahl = ms.nextInt();
		return zahl;
	
}
	public static double eingabeDouble (Scanner myScanner, String text) {
	 
		System.out.print(text);
		double zahl = myScanner.nextDouble();
		return zahl;
	}

	public static double mittelwertBerechnung(double zahl, int anzahl) {
		
		double m = zahl / anzahl;
		return m;
		
	}
	
	public static void ausgabe (double mittelwert) {
		System.out.println("Der Mittelwert beider Zahlen ist = " + mittelwert );
	}
}

