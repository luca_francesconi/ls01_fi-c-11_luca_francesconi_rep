import java.util.Scanner;
public class Quadrieren {

	public static void main(String[] args) {
		double x = eingabe();		
		double ergebnis = quadrieren(x);	
		ausgabe(x, ergebnis);
	}
	
	// (E) "Eingabe"
	// Wert f�r x festlegen:
	// ===========================
	public static double eingabe() {
		Scanner myScanner = new Scanner (System.in);
		System.out.println("Dieses Programm berechnet die Quadratzahl x�");
		System.out.println("---------------------------------------------");
		System.out.print("Geben Sie eine Zahl ein: ");
		double x = myScanner.nextDouble();
		return x; 
	}
	
	// (V) Verarbeitung
	// Mittelwert von x und y berechnen:
	// ================================
	public static double quadrieren (double zahl) {
		double ergebnis= zahl * zahl;	
		return ergebnis;
	}
	
	// (A) Ausgabe
	// Ergebnis auf der Konsole ausgeben:
	// =================================
	public static void ausgabe(double x, double ergebnis) {
		System.out.printf("x = %.2f und x�= %.2f\n", x, ergebnis);
	}
}