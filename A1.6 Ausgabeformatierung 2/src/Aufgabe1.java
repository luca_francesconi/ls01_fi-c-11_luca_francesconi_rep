
public class Aufgabe1 {

	public static void main(String[] args) {
		
		String a = "**";
		String b = "*";
		
		System.out.printf( "%5s\n", a );
		System.out.printf( "%-1s", b );
		System.out.printf( "%7s\n", b );
		System.out.printf( "%-1s", b );
		System.out.printf( "%7s\n", b );
		System.out.printf( "%5s", a );

	}

}
