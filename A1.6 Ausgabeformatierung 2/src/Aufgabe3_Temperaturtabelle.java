
public class Aufgabe3_Temperaturtabelle {

	public static void main(String[] args) {
		
		String a = "Fahrenheit";
		String b = "|";
		String c = "Celsius";
		String d = "------------------------";
		
		System.out.printf( "%-4s", a );
		System.out.printf( "%3s", b );
		System.out.printf( "%10s\n", c );
		System.out.printf( "%24s\n", d );
		
		
		double e = -20;
	    double f = -28.8889; 
		
	    System.out.printf( "%-1.3s", e);
	    System.out.printf( "%10s", b );
	    System.out.printf( "%10.6s\n", f-0.0011);
		
	    System.out.printf( "%-1.3s", e+10);
	    System.out.printf( "%10s", b );
	    System.out.printf( "%10.6s\n", f+5.5556);
		
	    System.out.printf( "+%-1.1s", e+20);
	    System.out.printf( "%11s", b );
	    System.out.printf( "%10.6s\n", f+11.1000);
	    
	    System.out.printf( "+%-2.2s", e+40);
	    System.out.printf( "%10s", b );
	    System.out.printf( "%10.5s\n", f+22.2100);
	    
	    System.out.printf( "+%-2.2s", e+50);
	    System.out.printf( "%10s", b );
	    System.out.printf( "%10.5s\n", f+27.7778);


	}

}
