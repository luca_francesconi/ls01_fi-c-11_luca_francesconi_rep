import java.util.Scanner;


public class Aufgabe1_For {

	public static void main(String[] args) {
		
		System.out.println("Bitte geben Sie eine Zahl ein: ");
		Scanner lucasca =  new Scanner(System.in);
		int nummer = lucasca.nextInt();
		hochzahlen(nummer);
		hochzahlen2(nummer);
		lucasca.close();
	}

		// Fall A = 1,2,3....
	public static void hochzahlen (int nummer) {
		System.out.println("\nDie erste Reihe ist: ");
		 for (int i = 1; i <= nummer ; i++) {
		    System.out.print(" " + i);
		 }
	}
	
		// Fall B = 3,2,1.....	
		 public static void hochzahlen2 (int nummer) { 	
			 System.out.println("\n\nDie zweite Reihe ist: ");
		    for (int i = nummer; i > 0; i--) {
			    System.out.print(" " + i);
			
		 	}
		}
}