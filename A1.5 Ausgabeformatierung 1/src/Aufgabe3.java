
public class Aufgabe3 {

	public static void main(String[] args) {
		
		double d = 22.4234234;
		double x = 111.2222;
		double a = 4.0;
		double e = 1000000.551;
		double f = 97.34;

		
		System.out.printf("%.7f %.2f\n" , d, d);
		System.out.printf("%.4f %.2f\n" , x, x);
		System.out.printf("%.1f %.2f\n" , a, a);
		System.out.printf("%.3f %.2f\n" , e, e);
		System.out.printf("%.2f %.2f\n" , f, f);
		

	}

}
