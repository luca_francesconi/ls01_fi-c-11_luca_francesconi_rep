﻿//Aufgabe A2.5: Anzahl der Tickets hinzufügen

//1 – Datentypen: double, int

//2 – Variable: zuZahlenderBetrag;  eingezahlterGesamtbetrag; eingeworfeneMünze; rückgabebetrag; tastatur;
                
//3 – Operationen: + und - 

import java.util.Scanner;

class Fahrkartenautomat
{
    public static void main(String[] args)
    {
    	Scanner tastatur = new Scanner(System.in);
        
        double zuZahlenderBetrag; 
        int zuZahlenderBetrag1;
        double eingezahlterGesamtbetrag;
        double eingeworfeneMünze;
        double rückgabebetrag;
        int anzahltickets;

        System.out.print("Zu zahlender Betrag (EURO): ");
        zuZahlenderBetrag = tastatur.nextDouble();
        zuZahlenderBetrag1 = (int) (zuZahlenderBetrag * 100);
        zuZahlenderBetrag = zuZahlenderBetrag1;
        
        System.out.print("Anzahl der Tickets: ");
        anzahltickets = tastatur.nextInt();
        
        zuZahlenderBetrag = anzahltickets*zuZahlenderBetrag;
        
        // Geldeinwurf
        // -----------
        eingezahlterGesamtbetrag = 0.0;
        while(eingezahlterGesamtbetrag < zuZahlenderBetrag)
        {
     	   System.out.printf("Noch zu zahlen: " + "%.2f Euro\n",(zuZahlenderBetrag/100 - eingezahlterGesamtbetrag/100));
     	   System.out.print("Eingabe (mind. 5Ct, höchstens 2 Euro): ");
     	   eingeworfeneMünze = tastatur.nextDouble();
     	   eingeworfeneMünze = eingeworfeneMünze * 100;
            eingezahlterGesamtbetrag += eingeworfeneMünze;
        }

        // Fahrscheinausgabe
        // -----------------
        System.out.println("\nFahrschein wird ausgegeben");
        for (int i = 0; i < 8; i++)
        {
           System.out.print("=");
           try {
 			Thread.sleep(250);
 		} catch (InterruptedException e) {
 			// TODO Auto-generated catch block
 			e.printStackTrace();
 		}
        }
        System.out.println("\n\n");

        // Rückgeldberechnung und -Ausgabe
        // -------------------------------
        rückgabebetrag = eingezahlterGesamtbetrag -zuZahlenderBetrag;
        if(rückgabebetrag > 0.0)
        {
     	   System.out.printf("Der Rückgabebetrag in Höhe von " + "%.2f Euro\n",rückgabebetrag/100);
     	   System.out.println("wird in folgenden Münzen ausgezahlt:");
     	   
            while(rückgabebetrag >= 200) // 2 EURO-Münzen
            {
         	  System.out.println("2 EURO");
 	          rückgabebetrag -= 200;
            }
            while(rückgabebetrag >= 100) // 1 EURO-Münzen
            {
         	  System.out.println("1 EURO");
         	  rückgabebetrag -= 100;
            }
            while(rückgabebetrag >= 50) // 50 CENT-Münzen
            {
         	  System.out.println("50 CENT");
 	          rückgabebetrag -= 50;
            }
            while(rückgabebetrag >= 20) // 20 CENT-Münzen
            {
         	  System.out.println("20 CENT");
  	          rückgabebetrag -= 20;
            }
            while(rückgabebetrag >= 10) // 10 CENT-Münzen
            {
         	  System.out.println("10 CENT");
 	          rückgabebetrag -= 10;
            }
            while(rückgabebetrag >= 5)// 5 CENT-Münzen
            {
         	  System.out.println("5 CENT");
  	          rückgabebetrag -= 5;
            }
            
        }
        tastatur.close();

        System.out.println("\nVergessen Sie nicht, den Fahrschein\n"+
                           "vor Fahrtantritt entwerten zu lassen!\n"+
                           "Wir wünschen Ihnen eine gute Fahrt.");
     }

}