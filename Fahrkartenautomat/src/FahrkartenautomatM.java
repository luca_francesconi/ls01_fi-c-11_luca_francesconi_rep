import java.util.Locale;
import java.util.Scanner;

class FahrkartenautomatM {
	static Scanner tastatur = new Scanner(System.in);
	
	
		public static void main(String[] args) {
			
			double gesamtbetrag = 0;
			int zuZahlenderBetrag1 = 0;
			double zuZahlenderBetrag = 0;
			
			do {
				
			// Fahrkartenbestellung Erfassen
			zuZahlenderBetrag =  fahrkartenbestellung_Erfassen();

			// Berechnung
			zuZahlenderBetrag1 = (int) (zuZahlenderBetrag * 100);
			zuZahlenderBetrag = zuZahlenderBetrag1;

			// Anzahl der Tickets
			if  (zuZahlenderBetrag != 0) {
				gesamtbetrag = gesamtbetrag + anzahl_der_Tickets(zuZahlenderBetrag);
				System.out.printf("Zwischensumme Berechnung: %.2f \n\n", gesamtbetrag /100);
				}
			} while (zuZahlenderBetrag != 0);
			
			// Fahrkarten Bezahlen
			double eingezahlterGesamtbetrag = 0.0;
			double r�ckgabebetrag = fahrkarten_Bezahlen(eingezahlterGesamtbetrag, gesamtbetrag);

			// Fahrschein Ausgabe
			if (gesamtbetrag>0) {
			fahrkarten_Ausgaben("\nFahrschein wird ausgegeben");

			// Wartezeit
			int millisekunde = 250;
			warte(millisekunde);
			
			// R�ckgeld Ausgaben
			rueckgeld_Ausgaben(r�ckgabebetrag);
			}
			// Scanner schliessen
			tastatur.close();
			// Benutzer Begr��ung
			begrue�ung();
			
		}


	// Fahrkartenbestellung Erfassen
	public static double fahrkartenbestellung_Erfassen() {
		System.out.println("W�hlen Sie Ihre Wunschfahrkarte f�r Belin AB aus: ");
		System.out.println("\n-- Einzelfahrschein Regeltarif AB [2,90 �] ---> W�hlen Sie die 1");
		System.out.println("-- Tageskarte Regeltarif AB [8,60 �] ---> W�hlen Sie die 2");
		System.out.println("-- Kleingruppen-Tageskarte Regeltarif AB [23,50 �] ---> W�hlen Sie die 3");
		System.out.println("-- Bezahlen ---> W�hlen Sie die 9");
		
		double zuZahlenderBetrag;
		while (true) {
			
		zuZahlenderBetrag = tastatur.nextDouble();
		
		if (zuZahlenderBetrag == 1) {
			zuZahlenderBetrag = 2.90;
			System.out.println("Sie haben Berlin AB Einzelfahrschein gew�hlt\n");
			break; }
		else if (zuZahlenderBetrag == 2) {
			zuZahlenderBetrag = 8.60;
			System.out.println("Sie haben Berlin AB Tageskarte gew�hlt\n");
			break; }
		else if (zuZahlenderBetrag == 3) {
			zuZahlenderBetrag = 23.50;
			System.out.println("Sie haben Berlin AB Kleingruppen-Tageskarte gew�hlt\n");
			break; }
		else if (zuZahlenderBetrag == 9) {
			zuZahlenderBetrag = 0;
			System.out.println("Sie haben Bezahlung gew�hlt");
			break; }
		
		else {
			System.out.printf("Ihre Wahl --> " + zuZahlenderBetrag);
			System.out.println(" ist Falsch!!! Bitte korrigieren Sie Ihre wahl! (nur 1 bis 3): ");
			
		   }
		}
		return zuZahlenderBetrag;
		
		
	}
	//Anzahl der Ticket
	public static double anzahl_der_Tickets(double zuZahlenderBetrag) {
		System.out.print("W�hlen Sie jetzt der Anzahl der Tickets: ");
		int anzahltickets = tastatur.nextInt();
		if (anzahltickets >0 && anzahltickets <10){
		zuZahlenderBetrag = anzahltickets * zuZahlenderBetrag;
		return zuZahlenderBetrag;
	}
		else {
			System.out.print("Achtung!!! Anzahl der Tickets soll zwischen 1 und 10 sein\n");
			anzahltickets = 1;
			System.out.print("Anzahl der Tickets wurde auf 1 gesetzt!!!\n");
			return zuZahlenderBetrag;
		}

}
	// Fahrkarten Bezahlen
	public static double fahrkarten_Bezahlen(double eingezahlterGesamtbetrag, double zuZahlenderBetrag) {

		while (eingezahlterGesamtbetrag < zuZahlenderBetrag) {
			System.out.printf(Locale.US, "\nNoch zu zahlen: %.2f %s \n",
					(zuZahlenderBetrag / 100 - eingezahlterGesamtbetrag / 100), "Euro");
			System.out.print("Eingabe (mind. 1Ct, h�chstens 2 Euro): ");
			double eingeworfeneM�nze = tastatur.nextDouble();
			eingeworfeneM�nze = eingeworfeneM�nze * 100;
			eingezahlterGesamtbetrag += eingeworfeneM�nze;
		}
		// R�ckgeldberechnung und -Ausgabe
		// -------------------------------
		double r�ckgabebetrag = eingezahlterGesamtbetrag - zuZahlenderBetrag;
		return r�ckgabebetrag;
	}

	// Fahrkarten Ausgaben

	public static void fahrkarten_Ausgaben(String fahrschein_Text) {

		System.out.println(fahrschein_Text);
	}

	// Wartezeit
	public static void warte(int millisekunde) {

		for (int i = 0; i < 8; i++) {
			System.out.print("=");

			try {
				Thread.sleep(millisekunde);
			}

			catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		System.out.println("\n\n");
	}

	// R�ckgeld Ausgaben
	public static void rueckgeld_Ausgaben(double r�ckgabebetrag) {
		if (r�ckgabebetrag > 0.0) {
			System.out.printf("Der R�ckgabebetrag in H�he von %.2f EURO ", r�ckgabebetrag / 100);
			System.out.println("wird in folgenden M�nzen ausgezahlt:");

			int r�ckgabebetrag1 = (int) r�ckgabebetrag;
			r�ckgabebetrag = r�ckgabebetrag1;

			// M�nze Ausgaben
			while (r�ckgabebetrag1 >= 200) {
				muenzeAusgeben(r�ckgabebetrag1, "2 EURO");
				r�ckgabebetrag1 -= 200;
			}
			while (r�ckgabebetrag1 >= 100) {
				muenzeAusgeben(r�ckgabebetrag1, "1 EURO");
				r�ckgabebetrag1 -= 100;
			}
			while (r�ckgabebetrag1 >= 50) {
				muenzeAusgeben(r�ckgabebetrag1, "50 CENT");
				r�ckgabebetrag1 -= 50;
			}
			while (r�ckgabebetrag1 >= 20) {
				muenzeAusgeben(r�ckgabebetrag1, "20 CENT");
				r�ckgabebetrag1 -= 20;
			}
			while (r�ckgabebetrag1 >= 10) {
				muenzeAusgeben(r�ckgabebetrag1, "10 CENT");
				r�ckgabebetrag1 -= 10;
			}
			while (r�ckgabebetrag1 >= 5) {
				muenzeAusgeben(r�ckgabebetrag1, "5 CENT");
				r�ckgabebetrag1 -= 5;
			}
			while (r�ckgabebetrag1 >= 2) {
				muenzeAusgeben(r�ckgabebetrag1, "2 CENT");
				r�ckgabebetrag1 -= 2;
			}
			while (r�ckgabebetrag1 >= 1) {
				muenzeAusgeben(r�ckgabebetrag1, "1 CENT");
				r�ckgabebetrag1 -= 1;
			}
		}

	}

	// M�nze Ausgaben
	public static void muenzeAusgeben(int betrag, String einheit) {
		System.out.println(einheit);
	}

	// Benutzer Begr��ung
	public static void begrue�ung() {
		System.out.println("\nVergessen Sie nicht, den Fahrschein\n" + "vor Fahrtantritt entwerten zu lassen!\n"
				+ "Wir w�nschen Ihnen eine gute Fahrt.");
	}
	
}