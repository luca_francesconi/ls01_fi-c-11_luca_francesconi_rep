import java.util.Scanner;

public class PCHaendler {
	
	public static void main(String[] args) {
		String bestellung;
		int anzahlbestellung;
		double nettopreis;
		double mwst;
		double gesamtnettopreis;
		double gesamtbruttopreis;
		
		Scanner myScanner = new Scanner(System.in);
		bestellung = eingabeString(myScanner, "was möchten Sie bestellen?");
		anzahlbestellung = eingabeInteger(myScanner, "Geben Sie die Anzahl ein: " );
		nettopreis = eingabeDouble(myScanner, "Geben Sie den Nettopreis ein:" );
		mwst = eingabe(myScanner, "Geben Sie den Mehrwertsteuersatz in Prozent ein:");
		gesamtnettopreis = gesamtnettoberechnung(anzahlbestellung, nettopreis);
		gesamtbruttopreis = gesamtbruttoberechnung(gesamtnettopreis, mwst);
		ausgabe(bestellung, anzahlbestellung, gesamtnettopreis, gesamtbruttopreis, mwst);
		
		myScanner.close();
	}
	
	public static String eingabeString(Scanner myScanner, String text) {
	
		System.out.print(text);
		String eingabe = myScanner.next();
		return eingabe;
	}
	
	public static int eingabeInteger(Scanner myScanner, String text) {
		
		System.out.print(text);
		int eingabe = myScanner.nextInt();
		return eingabe;
		
	}
	
	public static double eingabeDouble(Scanner myScanner, String text) {
		
		System.out.print(text);
		double eingabe = myScanner.nextDouble();
		return eingabe;
	}
	
    public static double eingabe(Scanner myScanner, String text) {
		
		System.out.print(text);
		double eingabe = myScanner.nextDouble();
		return eingabe;
	}
	
	public static double gesamtnettoberechnung(int anzahlbestellung, double
	nettopreis) {
	 			double gesamtnetto = (anzahlbestellung * nettopreis);
				return gesamtnetto;
	}
 			
	public static double gesamtbruttoberechnung(double gesamtnettoberechnung,
	double mwst) {
				double gesamtbrutto =(gesamtnettoberechnung * (1 + mwst / 100));
				return gesamtbrutto;
	}
	
	
	public static void ausgabe(String bestellung, int anzahlbestellung, double
	berechneGesamtnettopreis, double berechneGesamtbruttopreis,
	double mwst) {

	System.out.println("\tRechnung");
	System.out.printf("\t\t Netto:  %-20s %6d %10.2f %n", bestellung, anzahlbestellung, berechneGesamtnettopreis);
	System.out.printf("\t\t Brutto: %-20s %6d %10.2f (%.1f%s)%n", bestellung, anzahlbestellung, berechneGesamtbruttopreis, mwst, "%");

	}
}
		

		