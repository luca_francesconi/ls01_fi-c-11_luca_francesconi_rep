import java.util.Scanner;

public class Aufgabe3 {
    static int i;

    public static int lieferkosten(int i){
        // set i1 = 10 if i > 10
        int i1 = 0;
        if (i < 10) {
            i1 += 10;
        }
        return i1;
    }

    public static double pauschale(int i, int i1){
        // add i1 to d
        double d = (i * 10) + i1;
        // add MwSt
        double temp = d * 0.19;
        d += temp;
        return d;
    }

    
    public static void main(String[] args) {
        System.out.println("Rechnungsbetrag Bestellung PC-M�use\nPreis: 10�\n\nAnzahl PC-M�use");
        Scanner input = new Scanner(System.in);
        i = input.nextInt();
        input.close();
        int i1 = lieferkosten(i);
        double d = pauschale(i, i1);
        System.out.println("\nGegendstand: PC-Maus\nAnzahl: " + i);
        System.out.println("Lieferpauschale: " + i1 + "�\nGesamtpreis: " + d + "� (incl. MwSt.)");
    }

}