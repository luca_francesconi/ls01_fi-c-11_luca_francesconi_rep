import java.util.Scanner;

public class Aufgabe1 {
	public static void main(String[] args) {
		Scanner Myscanner = new Scanner(System.in);
		int zahl1 = Myscanner.nextInt();
		int zahl2 = Myscanner.nextInt();
		int zahl3 = Myscanner.nextInt();
		Myscanner.close();
		zahlGleich(zahl1, zahl2);
		groesserKleinerAbfrage(zahl1, zahl2);
		groesserOderElse(zahl1, zahl2);

		zahl1biggestValue(zahl1, zahl2, zahl3);
		zahlDreibiggestValue(zahl1, zahl2, zahl3);
		biggestValue(zahl1, zahl2, zahl3);

	}

	private static void zahlGleich(int zahl1, int zahl2) {
		if (zahl1 == zahl2)
			System.out.println("Beide zahlen sind gleich");

	}

	private static void groesserKleinerAbfrage(int zahl1, int zahl2) {
		if (zahl1 > zahl2 || zahl2 > zahl1) {
			System.out.println("Eine Zahl ist größer");
		}
	}

	private static void groesserOderElse(int zahl1, int zahl2) {
		if (zahl1 >= zahl2) {
			System.out.println("Eine Zahl1 ist größer");
		} else {
			System.out.println("2Zahl 3 ist am Größten");
		}

	}

	private static void zahl1biggestValue(int zahl1, int zahl2, int zahl3) {
		if (zahl1 > zahl2 && (zahl1 > zahl3)) {
			System.out.println("Zahl 1 ist größer als Zahl2 und 3");
		}
	}

	private static void zahlDreibiggestValue(int zahl1, int zahl2, int zahl3) {
		if (zahl3 > zahl2 || (zahl3 > zahl1)) {
			System.out.println("Zahl 3 ist größer als Zahl 1 und 2");
		}
	}

	private static void biggestValue(int zahl1, int zahl2, int zahl3) {
		if (zahl3 > zahl2 && (zahl3 > zahl1)) {
			System.out.println("Zahl 3 ist größer als Zahl 1 und 2");
		} else if (zahl1 > zahl2 && (zahl1 > zahl3)) {

			System.out.println("Zahl 1 ist größer als Zahl2 und 3");
		} else {
			System.out.println("Zahl2 ist die größte Zahl");
		}
	}
}